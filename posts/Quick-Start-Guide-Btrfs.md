---
title: 'Quick Start Guide Btrfs and Timeshift'
date: '2021-05-09'
---
## Introduction

This article is a brief step-by-step guide for who want manually setup a environment linux, like Arch Linux or Gentoo, with Btrfs filesystem that will be compatible with Timeshift snapshots and autocompression.

First of all, why use Btrfs instead ext4? Basically, Btrfs has a bunch of functionalities that become it's self a better alternative, like: subvolumes, snapshot, compression, defragmentation, deduplication and RAID.

Timeshift is a gui application that allows the user create very easy backups of a linux system. It has 2 kinds os backups: RSYNC and BTRFS. The RSYNC backup clone whe hole mounted system to a different partition, which is the better option to avoid data loss after a disk damage. But this kind of backup takes a lot of time to copy all files in all partition tho a different storage system and Btrfs snapshot cans solve this problem.

In a Btrfs filesystem, is possible to create a restore point and rollback to it in a few seconds with snapshots. Snapshots are subvolumes that share data and metadata with other subvolumes. This is made possible by Btrfs' Copy on Write (CoW) ability. Subvolumes is like a virtual subpartition that allows to swap to a snapshot very easily.

With Btrfs is possible compress on write and decompress com read. This can be very useful for reduce disk usage, increase service life of SSD and speedup read/write operations.

## How to Setup?
The first step is format the partition with Btrfs:
```sh
mkfs.btrfs -L mylabel /dev/partition
```
A very common mistake is install the system directly at the partition, but it will became the rollback process very difficult. First is necessary create a subvolume. If you made this mistake check the check the section of Troubleshooting.

For the Timeshift allow us use BTRFS snapshots, is needed to create a subvolume called @ that contains the root partition and a @home for the home partition, like Ubuntu. So:

```sh
btrfs subvolume snapshot ./ ./@
btrfs subvolume snapshot ./ ./@home
```

After this proceed with the normal installation.
The next step is to modify the fstab file to configure the Btrfs partitions and subvolumes:

```sh
UUID=xx /     btrfs subvol=@,compress-force=zstd      0 0
UUID=xx /home btrfs subvol=@home,compress-force=zstd  0 0
```

The `compress-force=zstd` tag compress all possible files with zstd algorithm, without check if the result size is lower than the starting size. Zstd, today is the best realtime compression algorithm. It was build from Facebook to fast lossless compression algorithm, targeting real-time compression scenarios at zlib-level and better compression ratios.

The last step is install and setup the Timeshift. After this you'll be able to create and restore, RSYNC and BTRFS backups. 

Is possible to list the subvolumes with the following commando:

```sh
btrfs subvolume list /
```

And will printed something like this:

```sh
ID 299 gen 23588 top level 5 path @
ID 300 gen 12665 top level 5 path timeshift-btrfs/snapshots/2021-05-04_11-51-57/@
```

## Troubleshooting
If you install the system without a subvolume @, you can create a snapshot named @, chroot it and reinstall the kernel. If you need more information, you can check the article in [Fedora Magazine](https://fedoramagazine.org/convert-your-filesystem-to-btrfs/).



## See also

For more information check these links bellow.

<https://github.com/facebook/zstd/>  
<https://wiki.archlinux.org/title/Btrfs>  
<https://fedoramagazine.org/convert-your-filesystem-to-btrfs/>  
<https://github.com/teejee2008/timeshift/>  
<https://wiki.gentoo.org/wiki/Btrfs>  