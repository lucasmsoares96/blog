import Head from 'next/head'
import Layout, { siteTitle } from '../components/layout'
import utilStyles from '../styles/utils.module.css'
import { getSortedPostsData } from '../lib/posts'
import Link from 'next/link'
import Image from 'next/image'
import Date from '../components/date'
import { GetStaticProps } from 'next'

export default function Home({
  allPostsData
}: {
  allPostsData: {
    date: string
    title: string
    id: string
  }[]
}) {
  return (
    <Layout home>
      <Head>
        <title>{siteTitle}</title>
      </Head> 
      <section className={utilStyles.headingMd}>
        <p>Hey guys!! My name is Lucas and I am currently in the 5th period of Computer Engineering at CEFET-MG in Divinópolis. I created this blog to record the interesting topics and the difficulties I encountered during my autonomous studies. I hope that here you will find the answer you are looking for.
          
Below are the links to my social media.
        </p>
        <p>This blog was built following the tutorial on the official website of the {' '}
          <a href="https://nextjs.org/learn">Next.js framework </a>.
        </p>
      </section>
      <section className={`${utilStyles.headingMd} ${utilStyles.padding1px}`}>
        <h2 className={utilStyles.headingLg}>Blog</h2>
        <ul className={utilStyles.list}>
          {allPostsData.map(({ id, date, title }) => (
            <li className={utilStyles.listItem} key={id}>
              <Link href={`/posts/${id}`}>
                <a>{title}</a>
              </Link>
              <br />
              <small className={utilStyles.lightText}>
                <Date dateString={date} />
              </small>
            </li>
          ))}
        </ul>
      </section>
      <footer>
        <a href="https://github.com/lucasmsoares96"><Image
          priority
          src="/icons/GitHub.png"
          height={50}
          width={50}
        />
        </a>
        {' '}
        <a href="https://www.linkedin.com/in/lucasmsoares96/"><Image
          priority
          src="/icons/Linkedin.png"
          height={50}
          width={58.8}
        /></a>
        {' '}
        <a href="https://www.youtube.com/channel/UCe08PibNO0_PFjUVIu5WcxQ"><Image
          priority
          src="/icons/Youtube.png"
          height={50}
          width={70.85}
        /></a>
      </footer>
    </Layout>
  )
}

export const getStaticProps: GetStaticProps = async () => {
  const allPostsData = getSortedPostsData()
  return {
    props: {
      allPostsData
    }
  }
}